function demarrerTimer(nbMinutes){
  const temps = nbMinutes * 60
  MiseAJourTemps(temps)
  const intervale = setInterval(diminuerDe1Seconde, 1000)
  return intervale
}

function diminuerDe1Seconde(){
  let nbSecondes = document.getElementById('timer').getAttribute('data-secondes')
  nbSecondes = parseInt(nbSecondes)
  nbSecondes = nbSecondes - 1
  MiseAJourTemps(nbSecondes)
    if(nbSecondes === 0){
      clearInterval(intervale)
      alert("Vous avez perdu")
      //Faire terminer le jeu ou rafraichir la page


    }

}

function MiseAJourTemps(nbSecondes) {
  let secondes = nbSecondes % 60
  const minutes = Math.floor(nbSecondes / 60)
    if(secondes < 10) {
      secondes = "0" + secondes
    }

  const timer = document.getElementById('timer')  
  timer.textContent = minutes + ":" + secondes
  timer.setAttribute('data-secondes', nbSecondes)

}


const intervale = demarrerTimer(5)